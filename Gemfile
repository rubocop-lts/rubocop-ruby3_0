# frozen_string_literal: true

# NOTE: This Gemfile is only relevant to local development.
#       It allows during local development:
#         - code coverage reports to be generated
#         - style linting to be run with RuboCop & extensions
#       All CI builds use files in gemfiles/*.
source "https://rubygems.org"

git_source(:github) { |repo_name| "https://github.com/#{repo_name}" }

# Specify actual dependencies in the *.gemspec
gemspec

# Only add to the set of gems from the gemspec when running on local.
# All CI jobs must use a discrete Gemfile located at gemfiles/*.gemfile. They will not use this Gemfile
if ENV["CI"].nil?
  ruby_version = Gem::Version.new(RUBY_VERSION)
  minimum_version = ->(version, engine = "ruby") { ruby_version >= Gem::Version.new(version) && engine == RUBY_ENGINE }
  coverage = minimum_version.call("3.0")
  linting = minimum_version.call("3.0")
  debugging = minimum_version.call("3.0")

  gem "pry", platforms: %i[mri jruby]

  platforms :mri do
    if linting
      gem "rubocop-md", "~> 1.0", require: false
      gem "rubocop-packaging", "~> 0.5", require: false
      gem "rubocop-performance", "~> 1.11", require: false
      gem "rubocop-rake", "~> 0.6", require: false
      gem "rubocop-rspec", "~> 2.19", require: false
      gem "rubocop-thread_safety", "~> 0.4", require: false
    end
    if coverage
      gem "codecov", "~> 0.6", require: false # For CodeCov
      gem "simplecov", "~> 0.21", require: false
      gem "simplecov-cobertura", require: false # XML for Jenkins
      gem "simplecov-json", require: false # For CodeClimate
      gem "simplecov-lcov", "~> 0.8", require: false
    end
    if debugging
      # Add `byebug` to your code where you want to drop to REPL
      gem "byebug"
      gem "pry-byebug"
    end
  end

  platforms :jruby do
    # Add `binding.pry` to your code where you want to drop to REPL
    gem "pry-debugger-jruby"
  end
end
