# frozen_string_literal: true

module Rubocop
  module Ruby30
    VERSION = "1.1.2"
  end
end
